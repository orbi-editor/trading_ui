#ifndef SIGNALER_H
#define SIGNALER_H

#include <QObject>
#include <QDateTime>

class Signaler : public QObject
{
    Q_OBJECT
    Q_PROPERTY(double gain      READ gain       NOTIFY signalerChanged)
    Q_PROPERTY(double winrate   READ winrate    NOTIFY signalerChanged)
    Q_PROPERTY(double profit    READ profit     NOTIFY signalerChanged)
    Q_PROPERTY(double trades    READ trades     NOTIFY signalerChanged)
    Q_PROPERTY(double days      READ days       NOTIFY signalerChanged)
    Q_PROPERTY(QDateTime lastTrade READ lastTrade  NOTIFY signalerChanged)
    Q_PROPERTY(QString comment  READ comment    NOTIFY signalerChanged)

public:
    explicit Signaler(QObject *parent = nullptr);

    static Signaler *generate(QObject *parent = nullptr);

    double gain() const {return m_gain;}
    double winrate() const {return m_winrate;}
    double profit() const {return m_profit;}
    int trades() const {return m_trades;}
    int days() const {return m_days;}
    QDateTime lastTrade() const {return m_lastTrade;}
    QString comment() const {return m_comment;}

    Q_INVOKABLE void plusButton();

signals:
    void signalerChanged();

private:
    double m_gain;
    double m_winrate;
    double m_profit;
    int m_trades;
    int m_days;
    QDateTime m_lastTrade;
    QString m_comment;
};

#endif // SIGNALER_H
