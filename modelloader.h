#ifndef MODELLOADER_H
#define MODELLOADER_H

#include <QObject>
#include <QVariantList>

class Torg;
class Signaler;

class ModelLoader : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QVariantList tradesModel READ tradesModel NOTIFY tradesModelChanged)
    Q_PROPERTY(QVariantList signalersModel READ signalersModel NOTIFY signalersModelChanged)
public:
    explicit ModelLoader(QObject *parent = nullptr);

    QVariantList tradesModel() const;
    QVariantList signalersModel() const;
    Q_INVOKABLE QVariantList loadNewsModel() const;

    void loadTorgs();
    void loadSignalers();

    void appendTorg(int id, Torg *torg);
    void changeTorg(int id);
    void changeSomeValue(int torgId, int value);

signals:
    void tradesModelChanged();
    void signalersModelChanged();

private:
    QMap<int, Torg*> m_trades;
    QMap<int, Signaler*> m_signalers;
};

#endif // MODELLOADER_H
