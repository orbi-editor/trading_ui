import QtQuick 2.9
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3

import CustomLib 1.1

Item {
    id: root

    readonly property var currentTrader: tradersModel[combobox.currentIndex]
    readonly property var tradersModel: [
        {"site": "IQOption.com",    "icon": "qrc:/icons/loginIQ3.png"},
        {"site": "Binary.com",      "icon": "qrc:/icons/loginBinary2.png"},
        {"site": "Spectre.ai",      "icon": "qrc:/icons/loginSpectre2.png"},
        {"site": "Alpari.fix",      "icon": "qrc:/icons/loginALP.png"}
    ]

    readonly property int spacer: 10


    ColumnLayout {
        anchors.fill: parent
        spacing: spacer

        GroupBox {
            Layout.fillWidth: true
            Text {
                anchors.fill: parent
                color: "grey"
                wrapMode: Text.Wrap
                font.pixelSize: mainWindow.smallTextSize
                text: qsTr("some text some text some text some text"
                           + " some text some text some text some text"
                           + " some text some text some text some text"
                           + " some text some text some text some text")
            }
        }

        ImageCombobox {
            id: combobox
            Layout.fillWidth: true
            model: tradersModel
            textRole: "site"
            imageRole: "icon"
        }

        Text {
            Layout.fillWidth: true
            text: qsTr("Account E-mail:")
        }
        TextField {
            id: emailText
            Layout.fillWidth: true
            placeholderText: qsTr("enter your email here")
        }

        Text {
            Layout.fillWidth: true
            text: qsTr("Password:")
        }
        TextField {
            id: passwordText
            Layout.fillWidth: true
            echoMode: TextInput.Password
            placeholderText: qsTr("enter your password here")
        }

        RowLayout {
            Layout.fillWidth: true
            spacing: spacer
            visible: accountProvider.autorizationProgress > 0.01
                     && accountProvider.autorizationProgress < 0.99

            ProgressBar {
                Layout.fillWidth: true
                value: accountProvider.autorizationProgress
            }

            Text {
//                Layout.fillWidth: true
                text: qsTr("Autorization. Please wait...")
            }
        }

        RowLayout {
            Layout.fillWidth: true
            spacing: spacer

            CheckBox {
                id: saveCombobox
                Layout.fillWidth: true
                checked: true
                text: qsTr("Save password")
            }

            Button {
                text: qsTr("Ok")
                onClicked: {
//                    var obj = {}
//                    obj.email = emailText.text
//                    obj.pass = passwordText.text
//                    obj.trader = currentTrader["site"]
//                    obj.savePass = saveCombobox.checked
//                    var str = ""
//                    for (var key in obj) {
//                        if (obj.hasOwnProperty(key))
//                            str += key + ": " + obj[key] +", "
//                    }

//                    console.log("ok pressed: " + str)
                    accountProvider.sendLogin(emailText.text,
                                              passwordText.text,
                                              saveCombobox.checked);
                }
            }

            Button {
                text: qsTr("Cancel")
                onClicked: {
                    accountProvider.cancelLogin();
                }
            }
        }

        Item {
            Layout.fillWidth: true
            Layout.fillHeight: true
        }

        GroupBox {
            Layout.fillWidth: true

            Text {
                anchors.fill: parent
                color: "grey"
                wrapMode: Text.Wrap
                font.pixelSize: mainWindow.smallTextSize
                text: qsTr("BY USING THIS SOFTWARE YOU ARE"
                           + " ACCEPTING ALL THE TERMS OF THE DISCLAIMER NOTICE."
                           + " IF YOU DO NOT AGREE WITH ANYTHING IN THE DISCLAMER"
                           + " YOU SHOULD NOT USE THIS SOFTWARE.")
            }
        }

        Button {
            Layout.fillWidth: true
            text: qsTr("DISCLAIMER")
        }
    }
}
