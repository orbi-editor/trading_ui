import QtQuick 2.8

Rectangle {
    property int barsRadius: 3
    property color barsColor: "white"

    width: 200
    height: 200
    color: "transparent"

    Rectangle {
        x: 0
        y: parent.height * .17
        width: parent.width
        height: parent.height * .16
        radius: barsRadius
        antialiasing: true
    }

    Rectangle {
        x: 0
        y: parent.height * .42
        width: parent.width
        height: parent.height * .16
        radius: barsRadius
        antialiasing: true
    }

    Rectangle {
        id: bar3
        x: 0
        y: parent.height * .67
        width: parent.width
        height: parent.height * .16
        radius: barsRadius
        antialiasing: true
    }
}
