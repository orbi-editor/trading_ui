#ifndef TORG_H
#define TORG_H

#include <QObject>
#include <QDateTime>

class Torg : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QDateTime opentime   READ opentime       NOTIFY torgChanged)
    Q_PROPERTY(QDateTime expirytime READ expirytime     NOTIFY torgChanged)
    Q_PROPERTY(QString asset        READ asset          NOTIFY torgChanged)
    Q_PROPERTY(double pl            READ pl             NOTIFY torgChanged)
    Q_PROPERTY(double openprice     READ openprice      NOTIFY torgChanged)
    Q_PROPERTY(double closeprice    READ closeprice     NOTIFY torgChanged)
    Q_PROPERTY(QString signalname   READ signalname     NOTIFY torgChanged)
    Q_PROPERTY(QString direction    READ direction      NOTIFY torgChanged)
    Q_PROPERTY(double tradeamount   READ tradeamount    NOTIFY torgChanged)
    Q_PROPERTY(QString comments     READ comments       NOTIFY torgChanged)

public:
    explicit Torg(QObject *parent = nullptr);

    static Torg *randomTorg(QObject *parent = nullptr);

    QString asset() const {return m_asset;}
    QDateTime opentime() const {return m_opentime;}
    QDateTime expirytime() const {return m_expirytime;}
    double pl() const {return m_pl;}
    double openprice() const {return m_openPrice;}
    double closeprice() const {return m_closePrice;}
//    double shared() const {return m_sharedTradeAmount;}
    QString signalname() const {return m_signalname;}
    QString direction() const {return m_direction_int == 0 ? "▲" : "▼";}
    double tradeamount() const {return m_tradeamount;}
    QString comments() const {return m_comments;}

signals:
    void torgChanged();

public slots:
    void setSomeValue(int value);

private:
    QDateTime m_opentime;
    QDateTime m_expirytime;
    double m_pl;
    double m_openPrice;
    double m_closePrice;
    QString m_signalname;
    QString m_signalid;
    int m_direction_int;
    double m_tradeamount;
//    QString m_direction; // 0 - UP , 1 - DOWN
    QString m_comments;
    QString m_asset;

    int expiry; // netto seconds // 57, 70...
    int expiryBrutto; // brutto seconds // 60, 300, ...


    double payoutPercentage;


    long long ticketid;
    long requestid;
    QString symbol;
    QString option;
    double m_sharedTradeAmount;


    bool isopened = false;
    bool isReal = false;
    QString brokerAccountType;
    QString brokerAccountID;
    int openedstatus;

    QString contractType;
    bool ticks = false;


//    QJsonObject trade;
    bool isError = false;
    QString status;

    bool isProviderTorg = false;
    double providerAmount;
    int re_trials = 0; // if "Time for purchasing options is over..." IQ Option

};

#endif // TORG_H
