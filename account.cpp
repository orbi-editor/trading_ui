#include "account.h"

Account::Account()
{
    clean();
}

void Account::clean()
{
    balance = 0;
    dayProfit = 0;
    openedVolume = 0;
}
