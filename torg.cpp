#include "torg.h"
#include "utils.h"

Torg::Torg(QObject *parent):
    QObject(parent)
{
}

Torg *Torg::randomTorg(QObject *parent)
{
    Torg *torg = new Torg(parent);
    torg->m_opentime = QDateTime::currentDateTime();
    torg->m_expirytime = QDateTime::currentDateTime().addSecs(10);
    torg->m_asset = "EURCHF";
    torg->m_direction_int = Utils::randomBool() ? 0 : 1;
    torg->m_tradeamount = Utils::random(0, 5, 2);
    torg->m_pl = Utils::random(0, 1, 2);
    torg->m_openPrice = Utils::random(1, 2, 6);
    torg->m_closePrice = Utils::random(1, 2, 6);
    torg->m_signalname= Utils::randomBool() ? "BinaryProfit" : "Professional Trader";
    torg->m_comments = "some comment text";
    return torg;
}

void Torg::setSomeValue(int value)
{
    m_opentime = QDateTime::currentDateTime();

    emit torgChanged();
}
