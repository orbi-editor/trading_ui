#ifndef ACCOUNT_H
#define ACCOUNT_H

#include <QObject>

class Account
{
    Q_GADGET
    Q_PROPERTY(double balance       MEMBER balance)
    Q_PROPERTY(double dayProfit     MEMBER dayProfit)
    Q_PROPERTY(double openedVolume  MEMBER openedVolume)

public:
    Account();
    void clean();

public:
    double balance;
    double dayProfit;
    double openedVolume;
};

Q_DECLARE_METATYPE(Account)

#endif // ACCOUNT_H
