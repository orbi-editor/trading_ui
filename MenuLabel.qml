import QtQuick 2.9

//Hamburger button
Rectangle {
    readonly property real itemHeight: height / 7.0
    readonly property color foregroundColor: "black"

    width: 35
    height: 35
    color: "transparent"

    Rectangle {
        x: 0
        y: itemHeight * 2
        width: parent.width
        height: itemHeight
        radius: itemHeight / 3
        color: foregroundColor
    }

    Rectangle {
        x: 0
        y: itemHeight * 4
        width: parent.width
        height: itemHeight
        radius: itemHeight / 3
        color: foregroundColor
    }

    Rectangle {
        x: 0
        y: itemHeight * 6
        width: parent.width
        height: itemHeight
        radius: itemHeight / 3
        color: foregroundColor
    }
}
