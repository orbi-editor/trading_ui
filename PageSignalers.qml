import QtQuick 2.9
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3

import CustomLib 1.1

import "qrc:/CustomControls" as Custom
import "Helper.js" as Helper

Item {
    id: root

    ColumnLayout {
        anchors.fill: parent

        spacing: 5

        RowLayout {
            Layout.fillWidth: true

            ComboBox {
                id: sortSignalersComboBox

                property string modelField: {
                    switch (currentIndex) {
                    case 0: return "gain";
                    case 1: return "winrate";
                    case 2: return "profit"
                    }
                }

                Layout.fillWidth: true

                displayText: qsTr("Sotr by: ") + currentText
                model: [
                    "Gain",
                    "Winrate",
                    "Profit share"
                ]
            }

            Custom.Button {
                id: orderButton

                property bool orderFlag: false

                iconSource: "qrc:/icons/sort_order.png"
                onClicked: orderFlag = !orderFlag
            }

            Custom.Button {
                iconSource: "qrc:/icons/reload.png"
            }

            Custom.Button {
                id: filterButton
                iconSource: "qrc:/icons/funnel.png"
                onClicked: {
                    signalersFilter.visible = !signalersFilter.visible
                }
            }
        }

        SignalersFilter {
            id: signalersFilter
            Layout.fillWidth: true
            Layout.preferredHeight: 450

            visible: false
            onApplied: {
                filterButton.checked = v
                signalersFilter.visible = false
            }
        }

        ListView {
            id: signalersView

            Layout.fillWidth: true
            Layout.fillHeight: true

            clip: true
            spacing: 5
            currentIndex: 0
            model: Helper.sortModelByField(modelLoader.signalersModel,
                                           sortSignalersComboBox.modelField)

            delegate: Loader {
                property int savedIndex: index
                property var savedItem: signalersView.model[index]
                property color delegateColor: (index % 2) ? "white" : "#c0c0c0"
                sourceComponent: index == signalersView.currentIndex
                                 ? componentFull
                                 : componentShort
                width: signalersView.width
            }
        }
    }

    Component {
        id: componentFull
        DelegateSignalerFull {
            signaler: savedItem
            onPlusPressed: {
                console.log("plus pressed for " + savedIndex)
                signaler.plusButton()
            }
        }
    }

    Component {
        id: componentShort
        DelegateSignalerShort {
            signaler: savedItem
            fieldText: sortSignalersComboBox.currentText
            fieldModel: sortSignalersComboBox.modelField
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    signalersView.currentIndex = savedIndex
                }
            }
        }
    }
}
