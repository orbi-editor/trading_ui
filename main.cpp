#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include "modelloader.h"
#include "accountprovider.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    qmlRegisterUncreatableType<ModelLoader>("CustomLib", 1, 1, "ModelLoader", "");
    qmlRegisterUncreatableType<AccountProvider>("CustomLib", 1, 1, "AccountProvider", "");
    qmlRegisterUncreatableType<Account>("CustomLib", 1, 1, "Account", "");

    QQmlApplicationEngine engine;

    ModelLoader modelLoader;
    engine.rootContext()->setContextProperty("modelLoader", &modelLoader);

    AccountProvider accountProvider;
    engine.rootContext()->setContextProperty("accountProvider", &accountProvider);

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    modelLoader.loadTorgs();
    modelLoader.loadSignalers();

    return app.exec();
}
