import QtQuick 2.9 as Quick
import QtQuick.Controls 2.5 as Controls

Quick.Text {
    font.pixelSize: mainWindow.smallTextSize
    textFormat: Text.StyledText
}
