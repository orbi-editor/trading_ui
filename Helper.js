.pragma library

function colorizedText(text, flag) {
    var color = flag ? "green" : "red"
    return "<font color='" + color + "'>" + text + "</color>"
}

function normDate(v) {
    if (v < 10)
        return "0" + v;

    return v;
}

function dateToDMY(d) {
    return normDate(d.getDate()) + "."
            + normDate(d.getMonth() + 1) + "."
            + d.getFullYear();
}

function dateToDMY_HMS(d) {
    return dateToDMY(d) + " "
            + normDate(d.getHours()) + ":"
            + normDate(d.getMinutes()) + ":"
            + normDate(d.getSeconds());
}

function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
}

function round(value, dec) {
    var m = Math.pow(10, dec);
    return Math.round(value * m) / m;
}

function sortModelByField(modelArray, field, minToMax) {
    if (minToMax === undefined)
        minToMax = true;

    modelArray.sort(function(a, b) {
        if (minToMax)
            return a[field] - b[field];
        else
            return b[field] - a[field];
    });

    return modelArray;
}

function filterByMinFields(modelArray, fieldPairs) {
    if (!fieldPairs || fieldPairs.length === 0
            || !modelArray || !modelArray.length === 0)
        return modelArray;

    var res = modelArray.filter(function(modelElement) {
        return Object.keys(fieldPairs).reduce(function(flag, fieldKey) {
            return flag && (modelElement[fieldKey] > fieldPairs[fieldKey]);
        }, true);
    });

    return res;
}
