#include "signaler.h"
#include "utils.h"

#include <QDebug>

Signaler::Signaler(QObject *parent):
    QObject(parent)
{
}

Signaler *Signaler::generate(QObject *parent)
{
    Signaler *res = new Signaler(parent);
    res->m_gain = Utils::random(30, 100, 0);
    res->m_winrate = Utils::random(20, 90, 2);
    res->m_profit = Utils::random(0, 100, 0);
    res->m_trades = Utils::random(1000, 1500, 0);
    res->m_days = Utils::random(0, 10, 0);
    res->m_lastTrade = QDateTime::currentDateTime();
    res->m_comment = "comment comment comment comment comment"
                     " comment comment comment comment comment"
                     " comment comment comment comment comment"
                     " comment comment comment";
    return res;
}

void Signaler::plusButton()
{
    qDebug() << "selected item with gain" << gain();
}
