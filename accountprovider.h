#ifndef ACCOUNTPROVIDER_H
#define ACCOUNTPROVIDER_H

#include "account.h"

#include <QObject>
#include <QTimer>

class AccountProvider : public QObject
{
    Q_OBJECT

    Q_PROPERTY(Account account READ account NOTIFY autorizationProgressChanged)
    Q_PROPERTY(double autorizationProgress READ autorizationProgress NOTIFY autorizationProgressChanged)
    Q_PROPERTY(bool autorizationSuccess READ autorizationSuccess NOTIFY autorizationProgressChanged)

public:
    explicit AccountProvider(QObject *parent = nullptr);

    Account account() const {return m_account;}

    double autorizationProgress() const {return m_progress;}
    void setAutorizationProgress(double progress);

    bool autorizationSuccess() const {return m_status;}
    void setAutorizationSuccess(bool status);

    Q_INVOKABLE void sendLogin(const QString &login, const QString &pass, bool savePassword);
    Q_INVOKABLE void cancelLogin();

signals:
    void autorizationProgressChanged();

private:
    Account m_account;
    double m_progress;
    bool m_status;
};

#endif // ACCOUNTPROVIDER_H
