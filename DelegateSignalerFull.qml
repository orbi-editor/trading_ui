import QtQuick 2.9
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3

import "qrc:/CustomControls" as Custom
import "Helper.js" as Helper

Custom.Frame {
    id: root

    property var signaler
    signal plusPressed()

    width: 400
    height: 200

    ColumnLayout {
        anchors.fill: parent

        RowLayout {
            Layout.fillWidth: true
            Layout.preferredHeight: 32
            Layout.maximumHeight: 32

            Rectangle {
                Layout.preferredWidth: 32
                Layout.fillHeight: true

                radius: width/2
                color: "#75d461"
            }

            Custom.Text {
                Layout.fillWidth: true
                Layout.fillHeight: true

                font.pixelSize: mainWindow.normalTextSize
                verticalAlignment: Text.AlignVCenter

                text: qsTr("DAILY Signaler")
            }

            Image {
                Layout.preferredWidth: 32
                Layout.minimumWidth: 32
                Layout.fillHeight: true

                source: "qrc:/icons/plus.png"
                fillMode: Image.Pad

                MouseArea {
                    anchors.fill: parent
                    onClicked: root.plusPressed()
                }
            }
        }

        GridLayout {
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.margins: 5

            flow: GridLayout.TopToBottom
            rows: 3
            columnSpacing: 20
            rowSpacing: 5

            Custom.Text {
                text: qsTr("Gain: ") + Helper.colorizedText(signaler.gain + "%", true)
            }

            Custom.Text {
                text: qsTr("Winrate: ") + Helper.colorizedText(signaler.winrate + "%", false)
            }

            Custom.Text {
                text: qsTr("Profit share: ") + Helper.colorizedText(signaler.profit + "%", true)
            }


            Custom.Text {
                text: qsTr("Trades: ") + signaler.trades
            }

            Custom.Text {
                text: qsTr("Days: ") + Helper.colorizedText(signaler.days, false)
            }

            Custom.Text {
                text: qsTr("Last trade: ") + Helper.dateToDMY_HMS(signaler.lastTrade)
            }
        }

        Rectangle {
            Layout.fillWidth: true
            Layout.fillHeight: true

            border.color: "black"
            border.width: 1
            color: "transparent"

            Custom.Text {
                anchors.fill: parent
                anchors.margins: 5
                wrapMode: Text.Wrap
                text: signaler.comment
                anchors.rightMargin: 18
            }
        }
    }
}
