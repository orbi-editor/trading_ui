import QtQuick 2.9
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3

import "qrc:/CustomControls" as Custom
import "Helper.js" as Helper

Custom.Frame {
    property var trader

    width: 400
    height: 50

    RowLayout {
        anchors.fill: parent

        Column {
            Rectangle {
                width: mainWindow.smallTextSize
                height: width
                color: "green"
                radius: width/2
            }

            Rectangle {
                width: mainWindow.smallTextSize
                height: width
                color: "red"
                radius: width/2
            }
        }

        Column {
            Custom.Text {
                text: trader.asset
            }

            Custom.Text {
                text: qsTr("Signaler name")
            }
        }

        Custom.Text {
            text: Helper.dateToDMY_HMS(trader.opentime)
        }

        Custom.Text {
            text: (trader.direction/* ? "▲" : "▼"*/)
                  + " " + Helper.round(trader.tradeamount, 2)
                  + " (" + Helper.round(trader.pl, 2) + ")"
        }
    }
}
