import QtQuick 2.9
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3

import "qrc:/CustomControls" as Custom
import "Helper.js" as Helper

Rectangle {
    id: element

    property var newsModel: null

    width: 400
    height: 60

    ColumnLayout {
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.leftMargin: 10
        anchors.rightMargin: 10
        anchors.verticalCenter: parent.verticalCenter

         Row {
             Layout.fillWidth: true
             spacing: 20

             Custom.Text {
                 text: "USD"
             }
             Custom.Text {
                 text: newsModel ? "***" : "*"
             }

             Custom.Text {
                 text: "Before: " + (newsModel ? newsModel.before : 0)
             }
             Custom.Text {
                 text: "After: " + (newsModel ? newsModel.after : 0)
             }

             Custom.Text {
                 text: Helper.dateToDMY_HMS(new Date())
             }
         }

         Custom.Text {
             Layout.fillWidth: true
             Layout.fillHeight: true
             wrapMode: Text.Wrap
             text: "some some Event some some Event some some Event"
         }
     }
}

