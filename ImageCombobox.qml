import QtQuick 2.9
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.1

ComboBox {
    id: control
    property string imageRole: "image"
    property int contentHeight: control.implicitBackgroundHeight - 5

    delegate: ItemDelegate {
        property int delegeteHeight: control.height
        width: control.width
        highlighted: control.highlightedIndex === index
        contentItem: Row {
            Image {
                width: delegeteHeight
                height: delegeteHeight
                fillMode: Image.PreserveAspectFit
                source: modelData[imageRole]
            }

            Text {
                height: delegeteHeight
                text: modelData[textRole]
                font: control.font
                color: control.currentIndex === index ? Material.accent : "black"
                elide: Text.ElideRight
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignLeft
            }
        }
    }

    contentItem: Item {
        height: control.height
        width: control.width

        Row {
            anchors.verticalCenter: parent.verticalCenter
            anchors.leftMargin: control.indicator.width
            rightPadding: control.indicator.width + control.spacing

            Image {
                height: contentHeight
                width: contentHeight
                fillMode: Image.PreserveAspectFit
                source: model[currentIndex][imageRole]
            }

            Text {
                height: contentHeight
                text: control.displayText
                font: control.font
                elide: Text.ElideRight
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignLeft
            }
        }
    }
}
