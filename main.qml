import QtQuick 2.9
import QtQuick.Controls 2.5
import QtQuick.Window 2.12

ApplicationWindow {
    id: mainWindow

    readonly property int smallTextSize: 12
    readonly property int normalTextSize: 15

    visible: true
    width: 393
    height: 714
    title: qsTr("App")
    header: ToolBar {
        contentHeight: toolButton.implicitHeight

        ToolButton {
            id: toolButton
            text: "\u2630"
            font.pixelSize: Qt.application.font.pixelSize * 1.6
            onClicked: {
                drawer.open()
            }
        }

        Label {
            text: drawer.menuItems[drawer.currentPage]
            anchors.centerIn: parent
        }
    }

    Drawer {
        id: drawer

        property int currentPage: 0
        property var menuItems: [
            "Login",
            "Traders",
            "Signalers",
            "News"
        ]

        width: mainWindow.width * 0.66
        height: mainWindow.height

        Column {
            anchors.fill: parent

            Repeater {
                model: drawer.menuItems

                ItemDelegate {
                    text: modelData
                    width: parent.width
                    onClicked: {
                        drawer.currentPage = index
                        mainView.setSource("Page" + modelData + ".qml")
                        drawer.close()
                    }
                }
            }
        }
    }

    Component.onCompleted: {
        console.log("screen: " + Screen.desktopAvailableWidth + "x" + Screen.desktopAvailableHeight)
        console.log("visibility: " + mainWindow.visibility)
    }

    Loader {
        id: mainView
        anchors.fill: parent
        anchors.margins: 10
        source: "PageLogin.qml"
    }
}
