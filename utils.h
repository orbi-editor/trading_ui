#ifndef UTILS_H
#define UTILS_H

class Utils
{
public:
    Utils() = delete;

    static double random(double low, double high, int dec);
    static bool randomBool();
};

#endif // UTILS_H
