#include "accountprovider.h"

#include <QDebug>

AccountProvider::AccountProvider(QObject *parent):
    QObject(parent),
    m_progress(0),
    m_status(false)
{
}

void AccountProvider::setAutorizationProgress(double progress)
{
    m_progress = progress;
    emit autorizationProgressChanged();
}

void AccountProvider::setAutorizationSuccess(bool status)
{
    m_status = status;
    emit autorizationProgressChanged();
}

void AccountProvider::sendLogin(const QString &login, const QString &pass, bool savePassword)
{
    qDebug() << "login:" << login << "pass" << pass;
    setAutorizationProgress(0.5);
    setAutorizationSuccess(false);
}

void AccountProvider::cancelLogin()
{
    qDebug() << "interupt";
    m_account.balance = 1000;
    m_account.dayProfit = 13;
    m_account.openedVolume = 22;
    setAutorizationProgress(0);
    setAutorizationSuccess(false);
}
