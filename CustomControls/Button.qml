import QtQuick 2.9
import QtQuick.Controls 2.5

import QtQuick.Controls.Material 2.12

Button {
    id: root

    property alias iconSource: img.source

    contentItem: Image {
        id: img
        anchors.centerIn: parent
        fillMode: Image.Pad
        opacity: enabled ? 1.0 : 0.3
    }
}
