import QtQuick 2.9
import QtQuick.Controls 2.5 as Controls

Controls.SpinBox {
    font.pixelSize: mainWindow.smallTextSize
    implicitHeight: 100
    implicitWidth: 150
}

//Controls.SpinBox {
//    id: control

//    property int rectSize: 25

//    contentItem: TextInput {
//        z: 2
//        text: control.textFromValue(control.value, control.locale)

//        font: control.font
//        color: "#21be2b"
//        selectionColor: "#21be2b"
//        selectedTextColor: "#ffffff"
//        horizontalAlignment: Qt.AlignHCenter
//        verticalAlignment: Qt.AlignVCenter

//        readOnly: !control.editable
//        validator: control.validator
//        inputMethodHints: Qt.ImhFormattedNumbersOnly
//    }

//    up.indicator: Rectangle {
//        x: control.mirrored ? 0 : parent.width - width
//        height: parent.height
//        implicitWidth: rectSize
//        implicitHeight: rectSize
//        color: control.up.pressed ? "#e4e4e4" : "#f6f6f6"
//        border.color: enabled ? "#21be2b" : "#bdbebf"

//        Text {
//            text: "+"
//            font.pixelSize: control.font.pixelSize * 2
//            color: "#21be2b"
//            anchors.fill: parent
//            fontSizeMode: Text.Fit
//            horizontalAlignment: Text.AlignHCenter
//            verticalAlignment: Text.AlignVCenter
//        }
//    }

//    down.indicator: Rectangle {
//        x: control.mirrored ? parent.width - width : 0
//        height: parent.height
//        implicitWidth: rectSize
//        implicitHeight: rectSize
//        color: control.down.pressed ? "#e4e4e4" : "#f6f6f6"
//        border.color: enabled ? "#21be2b" : "#bdbebf"

//        Text {
//            text: "-"
//            font.pixelSize: control.font.pixelSize * 2
//            color: "#21be2b"
//            anchors.fill: parent
//            fontSizeMode: Text.Fit
//            horizontalAlignment: Text.AlignHCenter
//            verticalAlignment: Text.AlignVCenter
//        }
//    }

//    background: Rectangle {
//        implicitWidth: 4 * rectSize
//        border.color: "#bdbebf"
//    }
//}
