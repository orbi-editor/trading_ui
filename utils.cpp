#include "utils.h"
#include <QRandomGenerator>


double Utils::random(double low, double high, int dec)
{
    auto r = QRandomGenerator::global()->generateDouble() * (high - low) + low;
    auto m = pow(10, dec);
    return round(r * m) / m;
}

bool Utils::randomBool()
{
    return QRandomGenerator::global()->generate() % 2 == 0;
}
