import QtQuick 2.9
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3

import "qrc:/CustomControls" as Custom
import "Helper.js" as Helper

Custom.Frame {
    id: root

    property var signaler
    property string fieldText
    property string fieldModel

    width: 400
    height: 54

    RowLayout {
        anchors.fill: parent

        Rectangle {
            Layout.preferredWidth: 32
            Layout.preferredHeight: 32

            radius: width/2
            color: "#75d461"
        }

        Custom.Text {
            font.pixelSize: mainWindow.normalTextSize
            verticalAlignment: Text.AlignVCenter

            text: qsTr("DAILY Signaler")
        }

        Item {
            Layout.fillWidth: true
            Layout.fillHeight: true
        }

        Custom.Text {
            text: fieldText + ": " + Helper.colorizedText(signaler[fieldModel] + "%", true)
        }
    }
}
