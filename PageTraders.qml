import QtQuick 2.9
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3

import CustomLib 1.1

import "qrc:/CustomControls" as Custom
import "Helper.js" as Helper

Item {
    id: root

    Dialog {
        id: cleanBalanceDialog

        anchors.centerIn: Overlay.overlay

        standardButtons: Dialog.Ok | Dialog.Cancel
        onAccepted: console.log("clean balance")
        onRejected: console.log("do not clean balance")

        modal: true
        title: qsTr("Balance")
        Label {
            text: qsTr("Clean today balance?")
        }
    }

    ColumnLayout {
        anchors.fill: parent

        ComboBox {
            Layout.fillWidth: true
            model: [
                "Practice account (ID2XXXXXXX2)",
                "Real account  (ID2XXXXXXX2)"
            ]
        }

        Frame {
            id: accountBox
            Layout.fillWidth: true

            ColumnLayout {
                anchors.fill: parent

                GridLayout {
                    Layout.fillWidth: true

                    columns: 2

                    Text {
                        font.pixelSize: mainWindow.smallTextSize
                        text: qsTr("Balance:")
                    }
                    Text {
                        Layout.fillWidth: true
                        horizontalAlignment: Text.AlignRight
                        font.bold: true
                        font.pixelSize: mainWindow.smallTextSize
                        text: accountProvider.account.balance + qsTr(" USD")
                    }

                    Text {
                        font.pixelSize: mainWindow.smallTextSize
                        text: qsTr("Day profit:")
                    }
                    Text {
                        Layout.fillWidth: true
                        horizontalAlignment: Text.AlignRight
                        color: "green"
                        font.pixelSize: mainWindow.smallTextSize
                        text: qsTr("2.53 USD")
                    }

                    Text {
                        font.pixelSize: mainWindow.smallTextSize
                        text: qsTr("Opened volume:")
                    }
                    Text {
                        Layout.fillWidth: true
                        horizontalAlignment: Text.AlignRight
                        font.pixelSize: mainWindow.smallTextSize
                        text: qsTr("6.00 USD")
                    }
                }

                Button {
                    Layout.alignment: Qt.AlignRight
                    text: qsTr("Clean")
                    onClicked: cleanBalanceDialog.open()
                }
            }
        }

        RowLayout {
            Layout.fillWidth: true

            ComboBox {
                id: sortComboBox

                Layout.fillWidth: true

                property string selectedField: fieldName(currentIndex)

                function fieldName(index) {
                    switch (index) {
                    case 0: return "pl";
                    case 1: return "tradeamount";
                    case 2: return "openprice";
                    }
                }

                displayText: qsTr("Sotr by: ") + currentText
                model: [
                    "P/L",
                    "Amount",
                    "Open price"
                ]
            }

            Custom.Button {
                id: orderButton

                property bool orderFlag: false

                iconSource: "qrc:/icons/sort_order.png"
                onClicked: orderFlag = !orderFlag
            }
        }

        ListView {
            id: tradersView

            Layout.fillWidth: true
            Layout.fillHeight: true

            clip: true
            spacing: 5
            model: Helper.sortModelByField(modelLoader.tradesModel,
                                           sortComboBox.selectedField,
                                           orderButton.orderFlag);
            delegate: Loader {
                property int savedIndex: index
                property var savedItem: tradersView.model[index]
                sourceComponent: index == tradersView.currentIndex
                                 ? traderFullDelegate
                                 : traderShortDelegate
                width: tradersView.width
            }
        }
    }

    Component {
        id: traderShortDelegate
        TradeDelegateShort {
            trader: savedItem
            MouseArea {
                anchors.fill: parent
                onClicked: tradersView.currentIndex = savedIndex
            }
        }
    }

    Component {
        id: traderFullDelegate
        TradeDelegateFull {
            trader: savedItem
        }
    }
}
