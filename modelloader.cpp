#include "modelloader.h"
#include "torg.h"
#include "signaler.h"
#include "utils.h"

#include <QVariantMap>
#include <QDateTime>


ModelLoader::ModelLoader(QObject *parent):
    QObject(parent)
{
}

QVariantList ModelLoader::tradesModel() const
{
    QVariantList res;
    for (auto i = m_trades.constBegin();
         i != m_trades.constEnd();
         ++i)
    {
        res << QVariant::fromValue(i.value());
    }

    return res;
}

QVariantList ModelLoader::signalersModel() const
{
    QVariantList res;
    for (auto i = m_signalers.constBegin();
         i != m_signalers.constEnd();
         ++i)
    {
        res << QVariant::fromValue(i.value());
    }

    return res;
}

QVariantList ModelLoader::loadNewsModel() const
{
    QVariantList model;
    for (int i = 0; i < 10; ++i) {
        QVariantMap obj;
        obj.insert("before", Utils::random(0, 30, 0));
        obj.insert("after", Utils::random(0, 30, 0));
        obj.insert("volatility", Utils::random(1, 3, 0));
        obj.insert("pair", "USD");
        obj.insert("event", "some event comment some event comment");
        obj.insert("time", QDateTime::currentDateTime());
        model.append(obj);
    }
    return model;
}

void ModelLoader::loadTorgs()
{
    for (int i = 0; i < 20; ++i)
    {
        m_trades.insert(i, Torg::randomTorg(this));
    }

    emit tradesModelChanged();
}

void ModelLoader::loadSignalers()
{
    for (int i = 0; i < 20; ++i)
    {
        m_signalers.insert(i, Signaler::generate(this));
    }

    emit signalersModelChanged();
}

void ModelLoader::appendTorg(int id, Torg *torg)
{
    m_trades.insert(id, torg);
    emit tradesModelChanged();
}

void ModelLoader::changeTorg(int id)
{
    if (!m_trades.contains(id))
        return;

    //change smth in torg

    //generate signal for this torg
    //view will be updated automaticaly
    emit m_trades.value(id)->torgChanged();
}

void ModelLoader::changeSomeValue(int torgId, int value)
{
    if (!m_trades.contains(torgId))
        return;

    //setSomeValue() method generates torgChanged() signal
    m_trades.value(torgId)->setSomeValue(value);
}
