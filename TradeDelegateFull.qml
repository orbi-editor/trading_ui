import QtQuick 2.9
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3

import "qrc:/CustomControls" as Custom
import "Helper.js" as Helper

Custom.Frame {
    id: root

    property var trader

    width: 400
    height: 200

    ColumnLayout {
        anchors.fill: parent

        RowLayout {
            Layout.fillWidth: true
            Layout.maximumHeight: 50
            Layout.preferredHeight: 50
            Layout.maximumWidth: root.width

            Column {
                Rectangle {
                    width: mainWindow.smallTextSize
                    height: width
                    color: "green"
                    radius: width/2
                }

                Rectangle {
                    width: mainWindow.smallTextSize
                    height: width
                    color: "red"
                    radius: width/2
                }
            }

            Column {
                Custom.Text {
                    text: trader.asset
                }

                Custom.Text {
                    text: qsTr("Signaler name")
                }
            }

            Column {
                Custom.Text {
                    text: Helper.dateToDMY_HMS(trader.opentime)
                }

                Custom.Text {
                    text: Helper.dateToDMY_HMS(trader.expirytime)
                }
            }

            Custom.Text {
                text: (trader.direction/* ? "▲" : "▼"*/)
                      + " " + Helper.round(trader.tradeamount, 2)
                      + " (" + Helper.round(trader.pl, 2) + ")"
            }
        }

        GridLayout {
            Layout.fillWidth: true

            flow: GridLayout.TopToBottom
            rows: 2

            Custom.Text {
                text: qsTr("Open price:")
            }

            Custom.Text {
                text: qsTr("Close price:")
            }

            Custom.Text {
                text: Helper.round(trader.openprice, 6)
            }

            Custom.Text {
                text: Helper.round(trader.closeprice, 6)
            }
        }

        Rectangle {
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.maximumWidth: root.width

            border.color: "black"
            border.width: 1
            color: "transparent"

            Custom.Text {
                anchors.fill: parent
                anchors.margins: 5
                wrapMode: Text.Wrap
                text: trader.comments
            }
        }
    }
}
