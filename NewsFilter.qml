import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.5
import QtQuick.Controls.Material 2.12

import "qrc:/CustomControls" as Custom

Custom.Frame {
    id: root

    signal accepted()
    signal rejected()

    property var filterModel: [
        {
            type: "Low",
            before: 0,
            after: 0
        },
        {
            type: "Middle",
            before: 0,
            after: 0
        },
        {
            type: "High",
            before: 0,
            after: 0
        }
    ]

    ColumnLayout {
        anchors.fill: parent
        spacing: 10

        Text {
            Layout.fillWidth: true
            wrapMode: Text.Wrap
            font.pixelSize: mainWindow.smallTextSize
            text: qsTr("Specify the time gaps in minutes"
                       + " for filtering news events depending"
                       + " on volatility")
        }

        ListView {
            id: newsFilterView
            Layout.fillWidth: true

            spacing: 4
            model: filterModel
            height: filterModel.length * 50
            delegate: Pane {
                Material.elevation: 4
                height: 50
                anchors.left: parent.left
                anchors.right: parent.right

                RowLayout {
                    anchors.fill: parent
                    Custom.Text {
                        Layout.fillWidth: true
                        text: modelData.type + qsTr(" Volatility")
                    }
                    Custom.Text {
                        Layout.preferredWidth: 100
                        text: qsTr("Before ") + modelData.before
                    }
                    Custom.Text {
                        Layout.preferredWidth: 100
                        text: qsTr("After ") + modelData.after
                    }
                }

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        volatilityPopup.modelIndex = index
                        volatilityPopup.open()
                    }
                }
            }
        }

        RowLayout {
            Layout.fillWidth: true

            Item {
                Layout.fillWidth: true
            }
            Button {
                text: qsTr("Ok")
                onClicked: root.accepted()
            }
            Button {
                text: qsTr("Cancel")
                onClicked: root.rejected()
            }
        }
    }

    Dialog {
        id: volatilityPopup

        property int modelIndex: 0

        anchors.centerIn: Overlay.overlay
        width: root.width * 0.8
        height: 250
        modal: true
        title: qsTr("Set ") + filterModel[modelIndex].type + qsTr(" Volatility")
        standardButtons: Dialog.Ok | Dialog.Cancel

        onAccepted: {
            filterModel[modelIndex].before = beforeTumbler.currentIndex;
            filterModel[modelIndex].after = afterTumbler.currentIndex;
            newsFilterView.model = filterModel;
        }

        GridLayout {
            anchors.fill: parent
            columns: 2

            Custom.Text {
                Layout.fillWidth: true
                Layout.preferredWidth: 1

                horizontalAlignment: Text.AlignHCenter
                text: qsTr("Before")
            }
            Custom.Text {
                Layout.fillWidth: true
                Layout.preferredWidth: 1

                horizontalAlignment: Text.AlignHCenter
                text: qsTr("After")
            }

            Tumbler {
                id: beforeTumbler
                Layout.fillWidth: true
                Layout.preferredWidth: 1
                Layout.fillHeight: true

                visibleItemCount: 5
                model: 20
                currentIndex: filterModel[volatilityPopup.modelIndex].before
            }
            Tumbler {
                id: afterTumbler
                Layout.fillWidth: true
                Layout.preferredWidth: 1
                Layout.fillHeight: true

                visibleItemCount: 5
                model: 20
                currentIndex: filterModel[volatilityPopup.modelIndex].after
            }
        }
    }
}
