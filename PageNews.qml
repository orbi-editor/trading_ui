import QtQuick 2.9
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3

import CustomLib 1.1

import "qrc:/CustomControls" as Custom
import "Helper.js" as Helper

Item {
    id: root

//    function loadNewsModel() {
//        var model = [];
//        for (let i = 0; i < 10; ++i) {
//            let elem = {"before": Helper.round(Helper.getRandomArbitrary(0, 30), 0),
//                        "after": Helper.round(Helper.getRandomArbitrary(0, 30), 2),
//                        "volatility": Helper.round(Helper.getRandomArbitrary(1, 3), 0),
//                        "pair": "USD",
//                        "event": "some event comment some event comment",
//                        "time": new Date()
//            };
//            model.push(elem);
//        }

//        return model;
//    }

    function updateNewsModel(model, filterModel) {
        for (let i = 0; i < model.length; ++i) {
            let volatilityIndex = model[i]["volatility"] - 1;
            model[i]["before"] = filterModel[volatilityIndex]["before"];
            model[i]["after"] = filterModel[volatilityIndex]["after"];
        }
        return model;
    }

    ColumnLayout {
        anchors.fill: parent

        RowLayout {
            Layout.fillWidth: true

            ComboBox {
                id: newsSortCbx
                property string modelField: {
                    switch (currentIndex) {
                    case 0: return "gain";
                    case 1: return "winrate";
                    case 2: return "profit";
                    }
                }

                Layout.fillWidth: true

                displayText: qsTr("Sotr by: ") + currentText
                model: [
                    "Time",
                    "Volatility"
                ]
            }

            Custom.Button {
                id: orderButton

                property bool orderFlag: false

                iconSource: "qrc:/icons/sort_order.png"
                onClicked: orderFlag = !orderFlag
            }

            Custom.Button {
//                Layout.alignment: Qt.AlignRight

                iconSource: "qrc:/icons/funnel.png"
                onClicked: {
                    newsFilter.visible = !newsFilter.visible
                }
            }
        }

        NewsFilter {
            id: newsFilter
            Layout.fillWidth: true
            visible: false

            onAccepted: {
                newsView.model = updateNewsModel(newsView.model, filterModel)
                visible = false;
            }
            onRejected: visible = false
        }

        Custom.Frame {
            Layout.fillWidth: true
            Layout.fillHeight: true

            ListView {
                id: newsView
                anchors.fill: parent

                clip: true
                spacing: 0
                model: modelLoader.loadNewsModel()
                delegate: NewsDelegate {
                    newsModel: newsView.model[index]
                    color: (index % 2) ? "transparent" : "#809a9898"
                    anchors {
                        left: parent.left
                        right: parent.right
                    }
                }
            }
        }
    }
}
