import QtQuick 2.9
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3

import "qrc:/CustomControls" as Custom

Custom.Frame {
    id: root

    property var filterModel

//    function spinValueToText(value, locale) {
//        return Number(value).toLocaleString(locale, {maximumFractionDigits: 2}) + "%";
//    }

    signal applied(var v)

    width: 350
//    height: 500

    ColumnLayout {
        anchors.fill: parent
        anchors.margins: 5
        RadioButton {
            text: qsTr("Show All")
        }

        Rectangle {
            Layout.fillWidth: true
            Layout.preferredHeight: 1
            color: "grey"
        }

        RadioButton {
            id: applyFiltersButton
            text: qsTr("Apply Filters")
        }

        GridLayout {
            Layout.leftMargin: 5

            enabled: applyFiltersButton.checked
            columns: 2

            CheckBox {
                id: gainCheck
                text: qsTr("Gain, % >")
            }
            SpinBox {
                id: gainSpin
                from: 0
                to: 100
                value: 50
            }

            CheckBox {
                id: winrateCheck
                text: qsTr("Winrate, % >")
            }
            SpinBox {
                id: winrateSpin
                from: 0
                to: 100
                value: 50
            }

            CheckBox {
                id: tradesCheck
                text: qsTr("Trades >")
            }
            SpinBox {
                id: tradesSpin
                from: 0
                to: 10000
                value: 100
            }

            CheckBox {
                id: daysCheck
                text: qsTr("Days >")
            }
            SpinBox {
                id: daysSpin
                from: 0
                to: 10000
                value: 100
            }

            CheckBox {
                text: qsTr("Free")
            }
        }

        Button {
            Layout.fillWidth: true

            text: qsTr("Ok")
            onClicked: {
                filterModel = {}
                filterModel["gain"] = gainCheck.checked ? gainSpin.value : gainSpin.from
                filterModel["winrate"] = winrateCheck.checked ? winrateSpin.value : winrateSpin.from
                filterModel["trades"] = tradesCheck.checked ? tradesSpin.value : tradesSpin.from
                filterModel["days"] = daysCheck.checked ? daysSpin.value : daysSpin.from
                root.applied(applyFiltersButton.checked)
            }
        }
    }
}
